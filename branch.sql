/*
Branch#
Branch_Addr
ISBN
Title
Author
Publisher
Num_copies
*/
CREATE TABLE IF NOT EXISTS books (
    book_ID INT NOT NULL PRIMARY KEY, 
    ISBN VARCHAR(20), 
    book_name TEXT, 
    book_edition INT, 
    author_ID INT, 
    number_of_copies INT, 
    published_year YEAR(4)
);

CREATE TABLE IF NOT EXISTS branch ( 
    branch_ID INT NOT NULL PRIMARY KEY, 
    branch_address VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS authors ( 
    author_ID INT NOT NULL PRIMARY KEY, 
    first_name TEXT,
    last_name TEXT
);

CREATE TABLE IF NOT EXISTS publishers ( 
    publisher_ID INT NOT NULL PRIMARY KEY, 
    first_name TEXT, 
    last_name TEXT
);

CREATE TABLE IF NOT EXISTS books_and_branches ( 
    books_and_branches_ID INT NOT NULL PRIMARY KEY, 
    book_ID INT,
    branch_ID INT,
    author_ID INT, 
    publisher_ID INT, 
    FOREIGN KEY (book_ID) REFERENCES books(book_ID), 
    FOREIGN KEY (branch_ID) REFERENCES branch(branch_ID), 
    FOREIGN KEY (publisher_ID) REFERENCES publishers(publisher_ID), 
    FOREIGN KEY (author_ID) REFERENCES authors(author_ID), 
);