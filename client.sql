/*
Client#
Name
Location
Manager#
Manager_name
Manager_location
Contract#
Estimated_cost
Completion_date
Staff#
Staff_name
Staff_location
*/

CREATE TABLE IF NOT EXISTS client ( 
    client_ID INT NOT NULL PRIMARY KEY, 
    first_name TEXT, 
    last_name TEXT, 
    contract_ID INT, 
    location_ID INT, 
    manager_ID INT, 
    FOREIGN KEY (manager_ID) REFERENCES manager(manager_ID), 
    FOREIGN KEY (location_ID) REFERENCES locations(location_ID)
);

CREATE TABLE IF NOT EXISTS manager ( 
    manager_ID INT NOT NULL PRIMARY KEY, 
    first_name TEXT, 
    last_name TEXT, 
    location_ID INT, 
    FOREIGN KEY (location_ID) REFERENCES locations(location_ID)

);

CREATE TABLE IF NOT EXISTS contracts ( 
    contract_ID INT PRIMARY KEY, 
    contract_name VARCHAR(20), 
    estimated_cost FLOAT, 
    completion_date DATE, 
    manager_ID INT, 
    FOREIGN KEY (manager_ID) REFERENCES manager(manager_ID)
);

CREATE TABLE IF NOT EXISTS staff (
    staff_ID INT PRIMARY KEY, 
    first_name TEXT, 
    last_name TEXT, 
    location_ID INT, 
    manager_ID INT, 
    FOREIGN KEY (manager_ID) REFERENCES manager(manager_ID), 
    FOREIGN KEY (location_ID) REFERENCES locations(location_ID)
);

CREATE TABLE IF NOT EXISTS locations ( 
    location_ID INT NOT NULL PRIMARY KEY, 
    location_name TEXT
);