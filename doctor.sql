/*
Doctor#
DoctorName
Secretary
Patient#
PatientName
PatientDOB
PatientAddress
Prescription#
Drug
Date
Dosage
*/

CREATE TABLE IF NOT EXISTS doctor ( 
    doctor_ID INT NOT NULL PRIMARY KEY, 
    first_name TEXT, 
    last_name TEXT, 
    secretary_ID INT, 
    FOREIGN KEY (secretary_ID) REFERENCES secretary(secretary_ID)
);

CREATE TABLE IF NOT EXISTS patient ( 
    patient_ID INT NOT NULL PRIMARY KEY, 
    first_name TEXT, 
    last_name TEXT, 
    DOB DATE, 
    address_ID INT, 
    prescription_ID INT, 
    doctor_ID INT, 
    secretary_ID INT, 
    FOREIGN KEY (address_ID) REFERENCES adress(address_ID), 
    FOREIGN KEY (prescription_ID) REFERENCES prescription(prescription_ID), 
    FOREIGN KEY (doctor_ID) REFERENCES doctor(doctor_ID), 
    FOREIGN KEY (secretary_ID) REFERENCES secretary(secretary_ID),
);

CREATE TABLE IF NOT EXISTS secretary ( 
    secretary_ID INT PRIMARY KEY, 
    first_name TEXT, 
    last_name TEXT
);

CREATE TABLE IF NOT EXISTS prescription ( 
    prescription_ID INT NOT NULL PRIMARY KEY, 
    date_of_prescription DATE, 
);

CREATE TABLE IF NOT EXISTS drugs ( 
    drug_ID INT NOT NULL PRIMARY KEY, 
    drug_name TEXT, 
    prescription_ID INT, 
    FOREIGN KEY (prescription_ID) REFERENCES prescription(prescription_ID)

);

CREATE TABLE IF NOT EXISTS dosage ( 
    dosage_ID INT NOT NULL PRIMARY KEY, 
    dosage FLOAT
);

CREATE TABLE IF NOT EXISTS drugs_and_dosage ( 
    drug_ID INT, 
    dosage_ID INT, 
    FOREIGN KEY (drug_ID) REFERENCES drugs(drug_ID), 
    FOREIGN KEY (dosage_ID) REFERENCES dosage(dosage_ID)
);

CREATE TABLE IF NOT EXISTS address ( 
    address_ID INT NOT NULL PRIMARY KEY, 
    address TEXT
);


